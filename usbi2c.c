#include <stdio.h>
#include <err.h>
#include <libusb.h>
#include "usbi2c.h"

libusb_context *ctx;
libusb_device_handle *handle;

int ui2c_open()
{
	int ret;

	if ((ret = libusb_init(&ctx)) < 0) {
		warn("LibUSB initialisation error");
		return ret;
	}

	if ((handle = libusb_open_device_with_vid_pid(ctx, USBVID, USBPID)) == NULL) {
		libusb_exit(ctx);
		return -1;
	}

	return ret;
}

void ui2c_close()
{
	if (handle)
		libusb_close(handle);

	if (ctx)
		libusb_exit(ctx);
}

ssize_t ui2c_write(uint16_t addr, uint16_t flags, unsigned char *buf, size_t count)
{
	uint8_t bmRequestType = (LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE | LIBUSB_ENDPOINT_OUT);
	uint8_t bRequest = CMD_I2C_IO | CMD_I2C_IO_BEGIN | CMD_I2C_IO_END;
	uint16_t wValue = flags;
	uint16_t wIndex = addr;
	unsigned char pstatus;
	int ret;

	if ((ret = libusb_control_transfer(handle, bmRequestType, bRequest, wValue, wIndex, buf, count, 2000)) != count)
		return ret;

	if ((ret = libusb_control_transfer(handle, bmRequestType, CMD_GET_STATUS, 0, 0, &pstatus, 1, 2000)) != 1)
		return -1;

	if (pstatus == STATUS_ADDRESS_NAK)
		return -1;

	return count;
}

ssize_t ui2c_read(uint16_t addr, uint16_t flags, unsigned char *buf, size_t count)
{
	uint8_t bmRequestType = (LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE | LIBUSB_ENDPOINT_IN);
	uint8_t bRequest = CMD_I2C_IO | CMD_I2C_IO_BEGIN | CMD_I2C_IO_END;
	uint16_t wValue = flags;
	uint16_t wIndex = addr;
	unsigned char pstatus;
	int ret;

	if ((ret = libusb_control_transfer(handle, bmRequestType, bRequest, wValue, wIndex, buf, count, 2000)) != count) {
		printf("USB read failed: %d/%zu\n", ret, count);
		return ret;
	}

	if ((ret = libusb_control_transfer(handle, bmRequestType, CMD_GET_STATUS, 0, 0, &pstatus, 1, 2000)) != 1) {
		printf("CMD_GET_STATUS failed\n");
		return -1;
	}

	if (pstatus == STATUS_ADDRESS_NAK) {
		printf("NAK\n");
		return -1;
	}

	return count;
}

// vim: ts=4 sw=4 noexpandtab

