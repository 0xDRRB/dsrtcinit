/*
 * Copyright (c) 2024 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <locale.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include <sys/ioctl.h>

#if defined(__FreeBSD__)
	#include <dev/iicbus/iic.h>
#elif defined(USEUSB)
	#include "usbi2c.h"
#else // default to GNU/Linux
	#include <linux/i2c-dev.h>
#endif

#define DS3231_ADDR  0x68

void printhelp(char *binname)
{
	printf("dsRTCinit v0.2 - Copyright (c) 2024 - Denis Bodor\nThis is free software with ABSOLUTELY NO WARRANTY.\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -d <FILE>   i2c bus device to use (not used on OpenBSD)\n");
	printf(" -w          write system date/time to RTC (default: read and display)\n");
	printf(" -u          use system UTC time (default: local time)\n");
	printf(" -y          display date in YMD format (default: DMY)\n");
	printf(" -h          show this help\n");
}

int main (int argc, char**argv)
{
	char *optdevice = NULL;
	int optwrite = 0;
	int optutc = 0;
	int optymd = 0;
	int retopt;
#if !defined(USEUSB)
	int fd;
#endif
	int i;
	uint8_t buf[10] = { 0 };
	uint8_t rbuf[10] = { 0 };
	time_t now;
	struct tm ts;
	char strday[10] = { 0 };

	setlocale(LC_ALL, "");

	while ((retopt = getopt(argc, argv, "hwyud:")) != -1) {
		switch (retopt) {
		case 'd':
			optdevice = strdup(optarg);
			break;
		case 'w':
			optwrite = 1;
			break;
		case 'y':
			optymd = 1;
			break;
		case 'u':
			optutc = 1;
			break;
		case 'h':
			printhelp(argv[0]);
			return EXIT_SUCCESS;
		default:
			printhelp(argv[0]);
			return EXIT_FAILURE;
		}
	}

#if defined(USEUSB)
	if (ui2c_open() < 0) {
		if (optdevice)
			free(optdevice);
		err(EXIT_FAILURE, "Unable to open USB device!");
	}
#else
	if (!optdevice)
		errx(EXIT_FAILURE, "Error ! You need to give me a i2c bus to use (-d)!");

	if ((fd = open(optdevice, O_RDWR)) < 0) {
		free(optdevice);
		err(EXIT_FAILURE, "Error opening device");
	}
#endif
	if(optdevice)
		free(optdevice);

#if !defined(__FreeBSD__) && !defined(USEUSB)
	if (ioctl(fd, I2C_SLAVE, DS3231_ADDR) < 0) {
		close(fd);
		err(EXIT_FAILURE, "Error changing slave address");
	}
#endif

#if defined(__FreeBSD__)
	struct iic_msg msg[2];
	struct iic_rdwr_data rdwr;

	// message 1
	msg[0].slave = DS3231_ADDR << 1;
	msg[0].flags = IIC_M_WR;
	msg[0].len = 1;
	msg[0].buf = buf;

	// message 2
	msg[1].slave = DS3231_ADDR << 1;
	msg[1].flags = IIC_M_RD;
	msg[1].len = 1;
	msg[1].buf = rbuf;

	// 2 messages
	rdwr.nmsgs = 2;
	rdwr.msgs = msg;

	msg[1].len = 7;
	if (ioctl(fd, I2CRDWR, &rdwr) < 0) {
		close(fd);
		err(EXIT_FAILURE, "Error talking to the device");
	}
#elif defined(USEUSB)
	buf[0] = 0;
	if (ui2c_write(DS3231_ADDR, 0x0000, buf, 1) != 1) {
		ui2c_close();
		err(EXIT_FAILURE, "Error writing to DS3231");
	}
	usleep(1500);
	if (ui2c_read(DS3231_ADDR, I2C_M_RD, rbuf, 7) != 7) {
		ui2c_close();
		err(EXIT_FAILURE, "Error talking to the device");
	}
#else
	buf[0] = 0;
	if (write(fd, buf, 1) != 1) {
		close(fd);
		err(EXIT_FAILURE, "Error writing to DS3231");
	}
	usleep(1500);
	if (read(fd, rbuf, 7) != 7) {
		close(fd);
		err(EXIT_FAILURE, "Error talking to the device");
	}
#endif

	printf("Raw RTC date/time:\n  ");
	for (i = 0; i < 7; i++)
		printf("%02x ", rbuf[i]);
	printf("\n");

	ts.tm_wday = rbuf[3] - 1;
	strftime(strday, sizeof(strday), "%a", &ts);

	printf("\nRTC date/time:\n");
	if (optymd) {
		printf("  Date: %s %04u/%02u/%02u (YYYY/MM/DD)\n",
				strday,
				((rbuf[6] & 0x0f) + ((rbuf[6] >> 4) & 0x0f) * 10) + 2000,
				(rbuf[5] & 0x0f) + ((rbuf[5] >> 4) & 0x01) * 10,
				(rbuf[4] & 0x0f) + (rbuf[4] >> 4) * 10);
	} else {
		printf("  Date: %s %02u/%02u/%04u (DD/MM/YYYY)\n",
				strday,
				(rbuf[4] & 0x0f) + (rbuf[4] >> 4) * 10,
				(rbuf[5] & 0x0f) + ((rbuf[5] >> 4) & 0x01) * 10,
				((rbuf[6] & 0x0f) + ((rbuf[6] >> 4) & 0x0f) * 10) + 2000);
	}
	printf("  Time: %02u:%02u:%02u\n",
			(rbuf[2] & 0x0f) + (rbuf[2] >> 4) * 10,
			(rbuf[1] & 0x0f) + (rbuf[1] >> 4) * 10,
			(rbuf[0] & 0x0f) + (rbuf[0] >> 4) * 10);

	time(&now);

	if (optutc)
		gmtime_r(&now, &ts);
	else
		localtime_r(&now, &ts);

	strftime(strday, sizeof(strday), "%a", &ts);

	printf("\nSystem date/time %s:\n", optutc ? "(UTC)" : "(local)");
	if (optymd) {
		printf("  Date: %s %04u/%02u/%02u (YYYY/MM/DD)\n",
				strday,
				ts.tm_year + 1900,
				ts.tm_mon + 1,
				ts.tm_mday);
	} else {
		printf("  Date: %s %02u/%02u/%04u (DD/MM/YYYY)\n",
				strday,
				ts.tm_mday,
				ts.tm_mon + 1,
				ts.tm_year + 1900);
	}
	printf("  Time: %02u:%02u:%02u\n",
			ts.tm_hour,
			ts.tm_min,
			ts.tm_sec);

	buf[0] = 0x00;
	buf[1] = ((uint8_t)ts.tm_sec % 10) | (((uint8_t)ts.tm_sec / 10) << 4);
	buf[2] = ((uint8_t)ts.tm_min % 10) | (((uint8_t)ts.tm_min / 10) << 4);
	buf[3] = ((uint8_t)ts.tm_hour % 10) | (((uint8_t)ts.tm_hour / 10) << 4);
	buf[4] = (uint8_t)ts.tm_wday + 1;
	buf[5] = ((uint8_t)ts.tm_mday % 10) | (((uint8_t)ts.tm_mday / 10) << 4);
	buf[6] = (((uint8_t)ts.tm_mon + 1) % 10) | ((((uint8_t)ts.tm_mon + 1) / 10) << 4);
	buf[7] = (((uint8_t)ts.tm_year - 100) % 10) | ((((uint8_t)ts.tm_year - 100) / 10) << 4);

	printf("\nNew raw RTC date/time:\n  ");
	for (i = 1; i < 8; i++)
		printf("%02x ", buf[i]);
	printf("\n");

	if (optwrite) {
#if defined(__FreeBSD__)
		msg[0].len = 8;
		rdwr.nmsgs = 1;

		if (ioctl(fd, I2CRDWR, &rdwr) < 0) {
			close(fd);
			err(EXIT_FAILURE, "Error writing to RTC");
		}
#elif defined(USEUSB)
		if (ui2c_write(DS3231_ADDR, 0x0000, buf, 8) != 8) {
			ui2c_close();
			err(EXIT_FAILURE, "Error writing to RTC");
		}
#else
		if (write(fd, buf, 8) != 8) {
			close(fd);
			err(EXIT_FAILURE, "Error writing to RTC");
		}
#endif
		printf("\nRTC updated.\n");
	}

#if defined(USEUSB)
	ui2c_close();
#else
	close(fd);
#endif

	return EXIT_SUCCESS;
}
