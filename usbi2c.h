#define USBVID 0x1c40
#define USBPID 0x0534

// FROM PICO FIRMWARE
#define CMD_ECHO			0
#define CMD_GET_FUNC		1
#define CMD_SET_DELAY		2
#define CMD_GET_STATUS		3
#define CMD_I2C_IO			4
#define CMD_SET_LED			8
#define CMD_I2C_IO_BEGIN	(1 << 0)
#define CMD_I2C_IO_END		(1 << 1)
#define STATUS_IDLE			0
#define STATUS_ADDRESS_ACK	1
#define STATUS_ADDRESS_NAK	2
#define I2C_M_RD			0x0001
#define I2C_M_TEN			0x0010
#define I2C_M_NOSTART		0x4000
#define I2C_M_REV_DIR_ADDR	0x2000
#define I2C_M_IGNORE_NAK	0x1000
#define I2C_M_NO_RD_ACK		0x0800

int ui2c_open();
void ui2c_close();
ssize_t ui2c_write(uint16_t addr, uint16_t flags, unsigned char *buf, size_t count);
ssize_t ui2c_read(uint16_t addr, uint16_t flags, unsigned char *buf, size_t count);

// vim: ts=4 sw=4 noexpandtab

