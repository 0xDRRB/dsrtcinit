# dsRTCinit

Just a little tool to read the date/time from an i2c bus RTC and optionally write the current system date/time to the RTC configuration (use `-w` for "write"). The aim is to provide a tool for quickly setting up an RTC so that it can then be integrated into a microcontroller-based project (RPi Pico, Arduino, ESP32, etc.).

This code works with GNU/Linux and FreeBSD and has been tested with a Maxim DS3231 RTC. It should also work with DS1337, DS1338 or DS1307, but has not been tested.

This is primarily intended for use with i2c buses accessible via USB adapters (such as CP2112, [I2C-Tiny-USB](https://github.com/harbaum/I2C-Tiny-USB) or [rp2040-i2c-interface](https://github.com/Nicolai-Electronics/rp2040-i2c-interface)) on systems with a suitable kernel driver and providing a `/dev/i2c-*` or `/dev/iic*` interface.

Systems that do not support these adapters, such as OpenBSD and NetBSD, can use this tool via libUSB. This is the default solution if these systems are detected by the `Makefile`. You can also force the use of libUSB by using `make USEUSB=yes`.

